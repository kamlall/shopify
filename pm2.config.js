'use strict';

module.exports = {
  apps: [
    {
      name: 'shopify',
      script: 'src/index.js',
      watch:  true,
      ignore_watch: [
        './node_modules', './.pm2'
      ],
      node_args: '--inspect=0.0.0.0:5858'
    }
  ]
};
