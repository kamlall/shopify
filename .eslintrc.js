'use strict';

module.exports = {
  'env': {
    'es6': true,
    'node': true
  },
  "parserOptions": {
    "ecmaVersion": 2017
  },
  'rules': {

    // Best-practices.js
    // Enforces getter/setter pairs in objects
    'accessor-pairs': 0,

    // treat var statements as if they were block scoped
    'block-scoped-var': 2,

    // specify the maximum cyclomatic complexity allowed in a program
    'complexity': [ 0, 15 ],

    // require return statements to either always or never specify values
    'consistent-return': 2,

    // specify curly brace conventions for all control statements
    'curly': [ 2, 'all' ],

    // require default case in switch statements
    'default-case': 2,

    // encourages use of dot notation whenever possible
    'dot-notation': 0,

    // enforces consistent newlines before or after dots
    'dot-location': [ 2, 'property' ],

    // require the use of === and !==
    'eqeqeq': 2,

    // make sure for-in loops have an if statement
    // FIXME: Eventually we would like to set it to 2
    'guard-for-in': 0,

    // disallow the use of alert, confirm, and prompt
    'no-alert': 2,

    // disallow use of arguments.caller or arguments.callee
    'no-caller': 2,

    // disallow lexical declarations in case/default clauses
    'no-case-declarations': 2,

    // disallow division operators explicitly at beginning of regular expression
    'no-div-regex': 2,

    // disallow else after a return in an if
    'no-else-return': 2,

    // disallow use of labels for anything other then loops and switches
    'no-labels': [ 2, { 'allowLoop': true, 'allowSwitch': true } ],

    // disallow comparisons to null without a type-checking operator
    'no-eq-null': 2,

    // disallow use of eval()
    'no-eval': 2,

    // disallow adding to native types
    'no-extend-native': 2,

    // disallow unnecessary function binding
    'no-extra-bind': 2,

    // disallow fallthrough of case statements
    'no-fallthrough': 2,

    // disallow use of leading or trailing decimal points in numeric literals
    'no-floating-decimal': 2,

    // disallow the type conversions with shorter notations
    'no-implicit-coercion': [ 2, { 'allow': [ '!!' ] } ],

    // disallow use of eval()-like methods
    'no-implied-eval': 2,

    // disallow this keywords outside of classes or class-like objects
    'no-invalid-this': 0,

    // disallow usage of __iterator__ property
    'no-iterator': 2,

    // disallow use of labeled statements
    'no-labels': 2,

    // disallow unnecessary nested blocks
    'no-lone-blocks': 2,

    // disallow creation of functions within loops
    'no-loop-func': 2,

    // disallow use of multiple spaces
    'no-multi-spaces': 2,

    // disallow use of multiline strings
    'no-multi-str': 2,

    // disallow reassignments of native objects
    'no-native-reassign': 2,

    // disallow use of negated expressions in conditions
    'no-negated-condition': 2,

    // disallow new operator when not part of the assignment or comparison
    'no-new': 2,

    // disallow use of new operator for Function object
    'no-new-func': 2,

    // disallows creating new instances of String,Number, and Boolean
    'no-new-wrappers': 2,

    // disallow use of (old style) octal literals
    'no-octal': 2,

    // disallow use of octal escape sequences in string literals, such as
    // var foo = 'Copyright \251';
    'no-octal-escape': 2,

    // disallow reassignment of function parameters
    // FIXME: Eventually we would like to set it to 2
    'no-param-reassign': 0,

    // disallow use of process.env
    'no-process-env': 0,

    // disallow usage of __proto__ property
    'no-proto': 2,

    // disallow declaring the same variable more then once
    'no-redeclare': 2,

    // disallow use of assignment in return statement
    'no-return-assign': 2,

    // disallow use of `javascript:` urls.
    'no-script-url': 0,

    // disallow comparisons where both sides are exactly the same
    'no-self-compare': 2,

    // disallow use of comma operator
    'no-sequences': 2,

    // restrict what can be thrown as an exception
    'no-throw-literal': 2,

    // disallow usage of expressions in statement position
    'no-unused-expressions': 2,

    // disallow unnecessary .call() and .apply()
    'no-useless-call': 2,

    // disallow unnecessary concatenation of strings
    'no-useless-concat': 2,

    // disallow use of void operator
    'no-void': 2,

    // disallow usage of configurable warning terms in comments: e.g. todo
    'no-warning-comments': [
      0, { 'terms': [ 'todo', 'fixme', 'xxx' ], 'location': 'start' }
    ],

    // disallow use of the with statement
    'no-with': 2,

    // require use of the second argument for parseInt()
    'radix': 2,

    // requires to declare all vars on top of their containing scope
    'vars-on-top': 2,

    // require immediate function invocation to be wrapped in parentheses
    'wrap-iife': [ 2, 'any' ],

    // require or disallow Yoda conditions
    'yoda': 2,

    // require JSDoc on every function
    'require-jsdoc': [ 1, {
      'require': {
        'FunctionDeclaration': true,
        'MethodDefinition': true,
        'ClassDeclaration': true
      }
    } ],

    // Error.js
    // disallow trailing commas in object literals
    'comma-dangle': [ 2, 'only-multiline' ],

    // disallow assignment in conditional expressions
    'no-cond-assign': [ 2, 'except-parens' ],

    // disallow use of console
    'no-console': 2,

    // disallow use of constant expressions in conditions
    'no-constant-condition': 2,

    // disallow control characters in regular expressions
    'no-control-regex': 2,

    // disallow use of debugger
    'no-debugger': 2,

    // disallow duplicate arguments in functions
    'no-dupe-args': 2,

    // disallow duplicate keys when creating object literals
    'no-dupe-keys': 2,

    // disallow a duplicate case label.
    'no-duplicate-case': 2,

    // disallow the use of empty character classes in regular expressions
    'no-empty-character-class': 2,

    // disallow empty statements
    'no-empty': 2,

    // disallow assigning to the exception in a catch block
    'no-ex-assign': 2,

    // disallow double-negation boolean casts in a boolean context
    'no-extra-boolean-cast': 1,

    // disallow unnecessary parentheses
    'no-extra-parens': [ 2, 'functions' ],

    // disallow unnecessary semicolons
    'no-extra-semi': 2,

    // disallow overwriting functions written as function declarations
    'no-func-assign': 2,

    // disallow function or variable declarations in nested blocks
    'no-inner-declarations': 2,

    // disallow invalid regular expression strings in the RegExp constructor
    'no-invalid-regexp': 2,

    // disallow irregular whitespace outside of strings and comments
    'no-irregular-whitespace': 2,

    // Disallow negating the left operand of relational operators
    'no-unsafe-negation': 2,

    // disallow the use of object properties of the global object
    // (Math and JSON) as functions
    'no-obj-calls': 2,

    // disallow multiple spaces in a regular expression literal
    'no-regex-spaces': 2,

    // disallow sparse arrays
    'no-sparse-arrays': 2,

    // disallow unreachable statements after a return, throw, continue, or
    // break statement
    'no-unreachable': 2,

    // disallow comparisons with the value NaN
    'use-isnan': 2,

    // ensure JSDoc comments are valid
    'valid-jsdoc': [ 2, {
      'prefer': {
        'returns': 'return'
      },
      'requireReturn': false,
      'matchDescription': '^[A-Z][\\s\\S]*\\.$',
      'preferType': {
        'Boolean': 'boolean',
        'String': 'string',
        'Number': 'number',
        'object': 'Object',
        'array': 'Array',
        'symbol': 'Symbol',
        'Null': 'null'
      }
    } ],

    // ensure that the results of typeof are compared against a valid string
    'valid-typeof': 2,

    // Avoid code that looks like two expressions but is actually one
    'no-unexpected-multiline': 2,

    // Legacy.js
    // specify the maximum depth that blocks can be nested
    'max-depth': [ 2, 4 ],

    // specify the maximum length of a line in your program
    'max-len': [ 2, 120, 4, { 'ignoreUrls': true } ],

    // limits number of params that can be used in the function declaration.
    'max-params': [ 2, 5 ],

    // specify the maximum number of statement allowed in a function
    'max-statements': [ 0, 8 ],

    // disallow use of bitwise operators
    'no-bitwise': 0,

    // disallow use of unary operators, ++ and --
    'no-plusplus': 0,

    // Node.js
    // enforce return after a callback
    'callback-return': 0,

    // enforces error handling in callbacks (node environment)
    'handle-callback-err': 0,

    // disallow mixing regular variable and require declarations
    'no-mixed-requires': [ 0, false ],

    // disallow use of new operator with the require function
    'no-new-require': 0,

    // disallow string concatenation with __dirname and __filename
    'no-path-concat': 0,

    // disallow process.exit()
    'no-process-exit': 0,

    // restrict usage of specified node modules
    'no-restricted-modules': 0,

    // disallow use of synchronous methods (off by default)
    'no-sync': 0,

    // Strict.js
    // babel inserts `'use strict';` for us
    'strict': [ 2, 'global' ],

    // Style.js
    // enforce spacing inside array brackets
    'array-bracket-spacing': [ 2, 'always' ],

    // enforce spacing inside of single-line blocks
    'block-spacing': [ 2, 'always' ],

    // enforce one true brace style
    'brace-style': [ 2, 'stroustrup', { 'allowSingleLine': true } ],

    // DO NOT require camel case names
    'camelcase': 0,

    // enforce spacing before and after comma
    'comma-spacing': [ 2, { 'before': false, 'after': true } ],

    // enforce one true comma style
    'comma-style': [ 2, 'last' ],

    // disallow padding inside computed properties
    'computed-property-spacing': [ 2, 'never' ],

    // enforces consistent naming when capturing the current execution context
    'consistent-this': [ 2, 'self' ],

    // enforce newline at the end of file, with no multiple empty lines
    'eol-last': 2,

    // require function expressions to have a name
    'func-names': 0,

    // enforces use of function declarations or expressions
    'func-style': 0,

    // this option enforces minimum and maximum identifier lengths (variable
    // names, property names etc.)
    'id-length': 0,

    // this option sets a specific tab width for your code
    // https://github.com/eslint/eslint/blob/master/docs/rules/indent.md
    'indent': [ 2, 2, {
      'SwitchCase': 1, 'VariableDeclarator': { 'var': 2, 'let': 2, 'const': 3 }
    } ],

    // specify whether double or single quotes should be used in JSX attributes
    // http://eslint.org/docs/rules/jsx-quotes
    'jsx-quotes': [ 2, 'prefer-double' ],

    // enforces spacing between keys and values in object literal properties
    'key-spacing': [ 2, {
      'beforeColon': false, 'afterColon': true, 'mode': 'minimum'
    } ],

    // enforces empty lines around comments
    'lines-around-comment': [ 2, {
      'beforeBlockComment': true,
      'beforeLineComment': true,
      'allowBlockStart': true,
      'allowObjectStart': true,
      'allowArrayStart': true
    } ],

    // disallow mixed 'LF' and 'CRLF' as linebreaks
    'linebreak-style': [ 2, 'unix' ],

    // specify the maximum depth callbacks can be nested
    'max-nested-callbacks': [ 2, 3 ],

    // require a capital letter for constructors
    'new-cap': [ 2, { 'newIsCap': true } ],

    // disallow the omission of parentheses when invoking a constructor with no
    // arguments
    'new-parens': 2,

    // allow/disallow an empty newline after var statement
    'newline-after-var': 0,

    // disallow use of the Array constructor
    'no-array-constructor': 2,

    // disallow use of the continue statement
    'no-continue': 0,

    // disallow comments inline after code
    'no-inline-comments': 0,

    // disallow if as the only statement in an else block
    'no-lonely-if': 0,

    // disallow mixed spaces and tabs for indentation
    'no-mixed-spaces-and-tabs': 2,

    // disallow multiple empty lines and only one newline at the end
    'no-multiple-empty-lines': [ 2, { 'max': 2, 'maxEOF': 1 } ],

    // disallow nested ternary expressions
    'no-nested-ternary': 2,

    // disallow use of the Object constructor
    'no-new-object': 2,

    // disallow space between function identifier and application
    'func-call-spacing': 2,

    // disallow the use of ternary operators
    'no-ternary': 0,

    // disallow trailing whitespace at the end of lines
    'no-trailing-spaces': 2,

    // disallow dangling underscores in identifiers
    'no-underscore-dangle': 0,

    // disallow the use of Boolean literals in conditional expressions
    'no-unneeded-ternary': 0,

    // require padding inside curly braces
    'object-curly-spacing': [ 2, 'always' ],

    // allow just one var statement per function
    'one-var': 0,

    // require assignment operator shorthand where possible or prohibit it
    // entirely
    'operator-assignment': [ 2, 'always' ],

    // enforce operators to be placed before or after line breaks
    'operator-linebreak': 0,

    // enforce padding within blocks
    'padded-blocks': 0,

    // require quotes around object literal property names
    'quote-props': [ 2, 'consistent' ],

    // specify whether double or single quotes should be used
    'quotes': [ 2, 'single', 'avoid-escape' ],

    // require identifiers to match the provided regular expression
    'id-match': 0,

    // enforce spacing before and after semicolons
    'semi-spacing': [ 2, { 'before': false, 'after': true } ],

    // require or disallow use of semicolons instead of ASI
    'semi': [ 2, 'always' ],

    // sort variables within the same declaration block
    'sort-vars': 0,

    // require a space before and after certain keywords
    'keyword-spacing': [ 2, { 'before': true, 'after': true, 'overrides': { } } ],

    // require or disallow space before blocks
    'space-before-blocks': 2,

    // require or disallow space before function opening parenthesis
    // https://github.com/eslint/eslint/blob/master/docs/rules/space-before-function-paren.md
    'space-before-function-paren': [ 2, 'never' ],

    // require or disallow spaces inside parentheses
    'space-in-parens': [ 2, 'never' ],

    // require spaces around operators
    'space-infix-ops': 2,

    // Require or disallow spaces before/after unary operators
    'space-unary-ops': [ 2, {
      'words': true,
      'nonwords': false
    } ],

    // require or disallow a space immediately following // or /* in a comment
    'spaced-comment': [ 2, 'always', {
      'exceptions': [ '-', '+' ],

      'markers': [ '=', '!' ]           // space here to support sprockets
    } ],

    // require regex literals to be wrapped in parentheses
    'wrap-regex': 0,

    // Variable.js
    // enforce or disallow variable initializations at definition
    'init-declarations': 0,

    // disallow the catch clause parameter name being the same as a variable
    // in the outer scope
    'no-catch-shadow': 0,

    // disallow deletion of variables
    'no-delete-var': 2,

    // disallow labels that share a name with a variable
    'no-label-var': 0,

    // disallow shadowing of names such as arguments
    'no-shadow-restricted-names': 2,

    // disallow declaration of variables already declared in the outer scope
    'no-shadow': 2,

    // disallow use of undefined when initializing variables
    'no-undef-init': 2,

    // disallow use of undeclared variables unless mentioned in
    // a /*global */ block
    'no-undef': 2,

    // disallow use of undefined variable
    'no-undefined': 2,

    // disallow declaration of variables that are not used in the code
    'no-unused-vars': [ 2, { 'vars': 'local', 'args': 'after-used' } ],

    // disallow use of variables before they are defined
    'no-use-before-define': [ 2, { 'functions': false, 'classes': false } ],

    /* New rules in ESLint v2.0+ */

    // enforce usage of return statement in callbacks of array’s methods
    'array-callback-return': 2,

    // blacklist certain identifiers to prevent them from being used
    'id-blacklist': [ 2,
      'e',
      'o',
      'el',
      'ele',
      'elem',
      'opt',
      'opts',
      'cb',
      'callback',
      'data',
      'error',
      'event',
      'func',
      'num',
      'that'
    ],

    // check and report the chained calls if there are no new lines after
    // each call or deep member access
    'newline-per-chained-call': 2,

    // if a loop contains no nested loops or switches, labeling is unnecessary
    'no-extra-label': 2,

    // disallow self-assignment
    'no-self-assign': 2,

    // ensure variables in loop condition are modified in the loop
    'no-unmodified-loop-condition': 2,

    // disallow unused labels
    'no-unused-labels': 2,

    // disallow whitespace between objects and their properties
    'no-whitespace-before-property': 2,

    // require a newline around variable declarations
    'one-var-declaration-per-line': [ 2, 'always' ],

    // disallow non-special characters in strings and regular expressions
    'no-useless-escape': 2,

    // allow only one statement per line
    'max-statements-per-line': 2,

    // Disallow control flow statements in finally blocks
    'no-unsafe-finally': 2,

    // Disallow empty functions
    'no-empty-function': 2,

    // Disallow assignment to native objects or read-only global variables
    'no-global-assign': 2,

    // Disallow variable and function declarations in the global scope
    'no-implicit-globals': 2,

    // Disallow redundant return statements
    'no-useless-return': 2,

    // Ensure there is a newline before a return
    'newline-before-return': 2,

    // Disallow mixes of different operators
    'no-mixed-operators': 2,

    //----------------------------
    // ES6 specific rules
    //----------------------------

    // Require super() calls in constructors when the class extends from another class
    'constructor-super': 2,

    // Disallow modifying variables of class declarations
    'no-class-assign': 2,

    // Disallow modifying variables that are declared using const
    'no-const-assign': 2,

    // Disallow duplicate name in class members
    'no-dupe-class-members': 2,

    // Disallow new operators with the Symbol object
    'no-new-symbol': 2,

    // Disallow use of this/super before calling super() in constructors.
    'no-this-before-super': 2,

    // Disallow generator functions that do not have ‘yield’
    'require-yield': 2,

    // Require space before/after arrow function’s arrow
    'arrow-spacing': [ 2, { 'before': false, 'after': true } ],

    // Suggest using arrow functions as callbacks
    'prefer-arrow-callback': 1,

    // Disallow arrow functions where they could be confused with comparisons
    'no-confusing-arrow': 2,

    // Disallow unnecessary computed property keys on objects
    'no-useless-computed-key': 2,

    // Disallow unnecessary constructor
    'no-useless-constructor': 2,

    // Disallow template literal placeholder syntax in regular strings
    'no-template-curly-in-string': 2,

    // Enforce that class methods utilize this
    'class-methods-use-this': 0,

    // Flag any empty patterns in destructured objects and arrays
    'no-empty-pattern': 2,

    // No duplicate imports
    'no-duplicate-imports': 2,

    // require let or const instead of var
    'no-var': 2,

    // Suggest using const
    'prefer-const': 1,

    // Suggest using rest parameters
    'prefer-rest-params': 1,

    // Suggest using the spread operator instead of .apply()
    'prefer-spread': 1,

    // Suggest using template literals instead of string concatenation
    'prefer-template': 1,

     // require symbol description
    'symbol-description': 2,

    //----------------------------
    // ES7 specific rules
    //----------------------------

    // Disallow await inside of loops
    'no-await-in-loop': 2,

    // Disallows unnecessary return await
    'no-return-await': 2,

    // Disallow async functions which have no await expression
    'require-await': 0
  }
};
