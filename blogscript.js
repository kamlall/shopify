#!/usr/bin/env node

// This script obtains the latest n posts, by default 5
// Example: node blogscript.js --limit=1

const request = require('request');
const argv = require('yargs').argv
 
const server = argv.server || 'http://localhost:3010';
const limit = argv.limit || 5;
const url = `${server}/posts?limit=${limit}`;

request(url, function (error, response, body) {
  if (error) {
    console.log('error:', error); // Print the error if one occurred
    return;
  }
  if (response && response.statusCode === 200) {
     console.log('body:', body); // Print the HTML for the Google homepage.
  }
});
