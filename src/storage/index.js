'use strict';

const { PostStorage } = require('./post');

/**
 * Create a storage interface for the app, given connection information.
 * @param  {Object} connection Any connections required (knex, etc.)
 * @return {Object} Storage object interface.
*/
function createStorage({ connection }) {
  return {
    Post: new PostStorage({ connection })
  };
}

module.exports = exports = createStorage;
