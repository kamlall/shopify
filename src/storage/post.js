'use strict';

const { Post } = require('../models');

/**
 * Storage class for the Post model.
 * @class
 */
class PostStorage {
  /**
   * Create an instance of Post storage.
   * @param  {Object} connection An object with any connections required (redis, knex, etc.)
   */
  constructor({ connection }) {
    this._connection = connection;
  }

  async create({ title, email, desc }) {


    const post = new Post({ title, email, desc });

    await this._connection.knex.write.insert(post).into('post');

    return post;
  }

  async list({limit}) {

    let posts = [];
    if (limit) {
      posts = await this._connection.knex.read.select().from('post').limit(limit).orderBy('created_at', 'desc');
    }
    else {
      posts = await this._connection.knex.read.select().from('post').orderBy('created_at', 'desc');
    }

    return posts;
  }

}

exports.PostStorage = PostStorage;
