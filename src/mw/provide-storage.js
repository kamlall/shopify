'use strict';

/**
 * Middleware that makes the storage objects available to
 * each Koa context.
 * @param  {Object}  storage Application storage interface
 * @return {Promise} Return promise from storage middleware
 */
function provideStorage(storage) {
  return async function storageProvider(ctx, next) {
    ctx.storage = storage;
    await next();
  };
}

module.exports = exports = provideStorage;
