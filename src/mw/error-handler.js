'use strict';

/**
 * Error handler middleware.
 * @return {Promise} Return promise from error handler middleware
 */
function errorHandler() {
  return async function(ctx, next) {
    try {
      await next();
    }
    catch (err) {
      ctx.status = err.status || 500;
      ctx.body = {
        errorStatus: ctx.status,
        errorMessage: err.message || err
      };
      ctx.app.emit('error', err, ctx);
    }
  };
}

module.exports = exports = errorHandler;
