'use strict';

const joi = require('joi');

/**
 * Validation middleware.
 * @param  {Object[]} schemas      Joi schemas
 * @param  {Object}   options      Joi validation options
 * @param  {Function} validationCb Callback to invoke after validation
 * @return {Promise} Return promise from storage middleware
 */
function validate(schemas, options, validationCb) {
  return async function(ctx, next) {
    for (const key in schemas) {
      const source = (key === 'params') ? ctx.params : ctx.request[key],
            result = joi.validate(source, schemas[key], options, validationCb);
      if (result.error) {
        const details = result.error.details.map((detail)=> detail.message).join('\n');
        ctx.throw(400, details);
      }
    }
    await next();
  };
}

module.exports = exports = validate;
