'use strict';

const config = require('config'),
      createStorage = require('./storage'),
      { createApp } = require('./app'),
      logger = require('./logger'),
      knex_write = require('knex')(require('../knexfile')),
      knex_read = require('knex')({
        client: 'mysql',
        connection: config.get('knex.mysql.read')
      }),
      storage = createStorage({ connection: {
        knex: {
          write: knex_write,
          read: knex_read,
        }
      } });

return createApp({ storage })
.then((app)=> {
  app.listen(config.get('port'), ()=> {
    // eslint-disable-next-line no-console
    console.log(`Launched on port ${config.get('port')}`);
  });
})
.catch((err)=> {
  const errMessage = 'Server Error: Failed to launch server';
  // eslint-disable-next-line no-console
  logger.error(errMessage, err);
});

