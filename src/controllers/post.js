'use strict';

class PostController {

  /**
   * Render form to submit a post.
   * @param  {KoaContext} ctx The Koa context
   */
  async renderAddPostForm(ctx) {
    await ctx.render('./views/posts/create.hbs');
  }

  /**
   * Create a post.
   * @param  {KoaContext} ctx The Koa context
   */
  async create(ctx) {
    const { title, email, desc } = ctx.request.body,
          post = await ctx.storage.Post.create({
            title,
            email,
            desc
          });

    switch (ctx.accepts([ 'json', 'html' ])) {
      case 'html': {
        await ctx.render('./views/index.hbs');
        break;
      }
      case 'json': {
        ctx.body = { post };
        break;
      }
      case 'default': {
        ctx.body = { post };
        break;
      }
    }
  }

  /**
   * Get all posts.
   * @param  {KoaContext} ctx The Koa context
   */
  async getPosts(ctx) {
    const posts = await ctx.storage.Post.list({ limit: ctx.query.limit });

    switch (ctx.accepts([ 'json', 'html' ])) {
      case 'html': {
        await ctx.render('./views/posts/list.hbs', { posts } );
        break;
      }
      case 'json': {
        ctx.body = { posts };
        break;
      }
      case 'default': {
        ctx.body = { posts };
        break;
      }
    }
  }
}

module.exports = exports = new PostController();
