'use strict';

/**
 * Model for a Post.
 */
class Post {
  /**
   * Create a Post model.
   * @param {string}    title Title 
   * @param {string}    email Email
   * @param {string}    desc  Description
   */
  constructor({ title, email, desc = '' }) {
    this.title = title;
    this.email = email;
    this.desc = desc;
  }
}

module.exports = exports = Post;
