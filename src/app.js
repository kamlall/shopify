'use strict';

const Koa = require('koa'),
      cors = require('koa-cors2'),
      { provideStorage, errorHandler } = require('./mw'),
      rtr = require('./routes'),
      bodyParser = require('koa-bodyparser'),
      logger = require('./logger'),
      views = require('koa-views');

/**
 * Create the application.
 * @return {Koa} Koa application instance
 */
async function createApp({ storage }) {
  const app = new Koa();

  app.on('error', (err)=> {
    logger.error('Server Error', err);
    // eslint-disable-next-line no-console
    console.error('Server Error ', err);
  });

  // Install middleware
  app.use(errorHandler());

  app.use(bodyParser());
  app.use(provideStorage(storage));
  app.use(cors());
  app.use(views(__dirname, {
    map: { hbs: 'handlebars' }
  }));

  // Install the routes
  app.use(rtr.routes());

  return app;
}

exports.createApp = createApp;
