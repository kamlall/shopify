'use strict';

const Router = require('koa-router'),
      rtr = new Router();

rtr.use('/', require('./home').routes());
rtr.use('/posts', require('./post').routes());

module.exports = exports = rtr;
