'use strict';

const Router = require('koa-router'),
      rtr = new Router(),
      joi = require('joi'),
      { validate } = require('../mw'),
      PostController = require('../controllers/post');

rtr.get('/submit', PostController.renderAddPostForm.bind(PostController));

rtr.post('/', validate({
      body: {
        title: joi.string().min(1).max(255).required(),
        email: joi.string().email().required(),
        desc: joi.string().allow('').min(3).max(1000).optional()
      },
      query: {
        limit: joi.number()
      }
    }), PostController.create.bind(PostController));

rtr.get('/', PostController.getPosts.bind(PostController));


module.exports = exports = rtr;
