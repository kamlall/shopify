'use strict';

const Router = require('koa-router'),
      rtr = new Router(),
      { validate } = require('../mw');

rtr.get('/', async (ctx)=> {
  await ctx.render('./views/index.hbs');
});

module.exports = exports = rtr;
