'use strict';

// Passenger assumes that app.js will be the main entrypoint
// for our application.  So, let's make it happy.
require('./src');
