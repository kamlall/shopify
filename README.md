# Shopify Test

# Overview 

# Directory Structure

```
_
├── README.md                      // This README.md file
├── package.json                   // npm packaging
├── docker-compose.development.yml // Development Docker compose file
├── Dockerfile.development         // Development Dockerfile
├── Dockerfile                     // Production Dockerfile
├── pm2.config.js                  // Development pm2 configuration
├── config                         // Application configuration per environment
├── knexfile.js                    // Knex database configuration
├── src                            // Application source code
│   ├── index.js                   // The core entry point for the application
│   ├── app.js                     // Application creator module 
│   ├── mw                         // Koa Middleware
│   ├── routes                     // Application Routes
│   ├── controllers                // Application controllers
│   ├── models                     // Application Models
│   └── storage                    // Application Storage
├── test                           // Application Testing
│   ├── integration                // Integration Tests
│   └── unit                       // Unit Tests
├── db                             // Database related migration/seed scripts

```

# Getting Started 

The easiest and recommended way to setup the Shopify test is using Docker Compose. 

## Obtain the code

```shell
# Navigate to wherever you like to keep your source code
cd /path/to/your/source/code/folder

# Clone the repo
## Install Docker

https://www.docker.com/

## Run Docker Compose

```shell
# Navigate to the webapp folder
cd /path/to/your/source/code/folder/webapp

# Run docker compose 
docker-compose -f <docker-compose file> up --build -d
```

This should start the server and all dependencies such as MySQL in watch mode. Any code changes should restart the server.

Read up on docker and docker-compose to get familiar with various commands. For example, you can view server logs by issuing the command:

```
  docker logs -f webapp_shopify_service_1
```

You can log into a container by doing: 

```
  docker exec -i -t  shopify_shopify_service_1 /bin/bash
```

# Running Unit Tests

Log into the container.

Then run: 

```shell
npm run test
```

If you wish to also include code coverage information, then run: 

```shell
npm run testc
```

It is recommended that you run unit tests in TDD mode to autorun tests as you change code. 

```shell
npm run tdd
```

# Running Integration Tests

Log into the container and run the following command to setup databases:

```shell
npm run on_deploy
```

The run:

```shell
npm run itest
```

If you wish to also include code coverage information, then run: 

```shell
npm run itestc
```

# Linting Code

```shell
npm run lint
```

# Database migrations

First, see http://knexjs.org/#Migrations and then have a look at package.json for any knex related scripts such as:

```
    "knex:migrate:make": "knex migrate:make",
    "knex:migrate:latest": "knex migrate:latest",
    "knex:migrate:rollback": "knex migrate:rollback",
```
