'use strict';

exports.up = async function(knex /* ,Promise */) {
  let tableExists = await knex.schema.hasTable('post');
  if (!tableExists) {
    await knex.schema.createTable('post', (table)=> {
      table.increments();
      table.string('title', 255).notNullable();
      table.string('email', 255).notNullable();
      table.string('desc', 1000).notNullable();
      table.timestamps(false, true);
    });
  }
};

exports.down = function(knex /* ,Promise */) {
  return knex.schema.dropTable('post');
};
