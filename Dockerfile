FROM node:8

COPY webapp /home/node/webapp

# Install app dependencies
RUN chown -R node: /home/node/webapp; \
    su -l -c "cd /home/node/webapp; npm install" node
