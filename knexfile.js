'use strict';

const path = require('path'),
      config = require('config');

module.exports = {
  client: 'mysql',
  connection: config.get('knex.mysql.write'),
  migrations: {
    directory: path.join(__dirname, 'db', 'migrations'),
    tableName: 'shopify_migrations'
  },
  seeds: {
    directory: path.join(__dirname, 'db', 'seeds')
  }
};
