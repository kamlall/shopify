'use strict';

module.exports = {
  port: process.env.SERVICE_PORT,
  knex: {
    mysql: {
      read: {
        debug: false,
        host: process.env.MYSQL_READ_HOST,
        port: process.env.MYSQL_READ_PORT,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PWD,
        database: process.env.MYSQL_DATABASE
      },
      write: {
        debug: false,
        host: process.env.MYSQL_HOST,
        port: process.env.MYSQL_PORT,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PWD,
        database: process.env.MYSQL_DATABASE
      }
    }
  }
};
