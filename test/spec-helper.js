'use strict';

const knexCleaner = require('knex-cleaner'),
      path = require('path'),
      src = {
        root: path.resolve(path.join(__dirname, '..', 'src')),
        join(p) {
          return path.join(this.root, p);
        },
        require(p) {
          return require(this.join(p));
        },
      };

/**
 * Delete all storage data.
 * @param {Object} knex Knex client
 */
async function cleanStorage(knex) {
  await Promise.all([
    knexCleaner.clean(knex)
  ]);
}

module.exports = exports = {
  src: src,
  cleanStorage: cleanStorage,
};
