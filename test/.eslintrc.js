'use strict';

module.exports = {
  'rules': {
    'no-unused-expressions': 0,
  },
  'env': {
    'commonjs': true,
    'mocha': true
  }
};
