'use strict';

const posts = [
  {
    'title': 'Post 1',
    'email': 'foo@foo.com',
    'desc': 'This is description 1',
  },
  {
    'title': 'Post 2',
    'email': 'bar@bar.com',
    'desc': 'This is description 2',
  },
  {
    'title': 'Post 3',
    'email': 'bizz@bizz.com',
    'desc': 'This is description 3',
  }
];

module.exports = exports = {
  posts: posts
};
