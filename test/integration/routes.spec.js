'use strict';

const config = require('config'),
      chai = require('chai'),
      _ = require('lodash'),
      expect = chai.expect,
      sinonChai = require('sinon-chai'),
      supertest = require('supertest'),
      { src, cleanStorage } = require('../spec-helper'),
      mockModels = require('./mock-models'),
      { createApp } = src.require('app'),
      createStorage = src.require('./storage'),
      knex_write = require('knex')(require('../../knexfile')),
      knex_read = require('knex')({
        client: 'mysql',
        connection: config.get('knex.mysql.read')
      });

chai.use(sinonChai);

let request,
    storage;

describe('Routes', async ()=> {
  before(async ()=> {
    await cleanStorage(knex_write);

    storage = createStorage({
      connection: {
        knex: {
          write: knex_write,
          read: knex_read,
        }
      }
    });

    // eslint-disable-next-line one-var
    const app = await createApp({ storage: storage }),

          // Add 1 to avoid conflicts with an already running instance of the server
          server = await app.listen(config.get('port') + 1);

    request = supertest(server);
  });
  testPostRoutes();
});

/**
 * Test routes for Posts.
 */
function testPostRoutes() {
  describe('POST /posts/submit', ()=> {
    it('should create a post', async ()=> {
      const req = _.cloneDeep(mockModels.posts[0]),
            result = await request
              .post('/posts')
              .send(req)
              .expect(200);

       expect(result.body).to.deep.equal({ post: req });
    });
    it('should create a 2nd post', async ()=> {
      const req = _.cloneDeep(mockModels.posts[1]),
            result = await request
              .post('/posts')
              .send(req)
              .expect(200);
       
      expect(result.body).to.deep.equal({ post: req });
    });
    it('should create a 3rd post', async ()=> {
      const req = _.cloneDeep(mockModels.posts[2]),
            result = await request
              .post('/posts')
              .send(req)
              .expect(200);
      
      expect(result.body).to.deep.equal({ post: req });
    });
  });
  describe('GET /posts', ()=> {
    it('should retrieve all posts', async ()=> {
      const expectedposts = _.map(mockModels.posts, (tx, i)=> Object.assign({}, tx, { id: i + 1 })),
            result = await request
             .get('/posts')
             .send()
             .expect(200);

      const filteredResult = _.map(result.body.posts, (post)=> _.omit(post, [ 'created_at', 'updated_at' ]));
      expect(filteredResult).to.deep.equal(expectedposts);
    });
  });
}
